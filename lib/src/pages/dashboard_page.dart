import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _myBackground(),
          SingleChildScrollView(
            child: Column(
              children: [
                _myHeader(),
                _myMenuRonded(),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: _mtBottonNavigationBar(context),
    );
  }

  Widget _myBackground() {
    final myGradient = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset(0.0, 0.4),
              end: FractionalOffset(0.0, 1.0),
              colors: [
            Color.fromRGBO(54, 57, 113, 1.0),
            Color.fromRGBO(35, 37, 57, 1.0)
          ])),
    );

    final pinkBox = Transform.rotate(
      angle: -pi / 4.0,
      child: Container(
        width: 380.0,
        height: 380.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(90.0),
            gradient: LinearGradient(
                begin: FractionalOffset(1.0, 0.3),
                end: FractionalOffset(0.3, 1.0),
                colors: [
                  Color.fromRGBO(255, 131, 171, 1.0),
                  Color.fromRGBO(254, 75, 196, 1.0)
                ])),
      ),
    );

    return Stack(
      children: [
        myGradient,
        Positioned(top: -100.0, left: -15.0, child: pinkBox)
      ],
    );
  }

  Widget _myHeader() {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Classify transaction',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 5.0),
            Text('Classify this transaction into a particular category',
                style: TextStyle(color: Colors.white, fontSize: 20.0))
          ],
        ),
      ),
    );
  }

  Widget _mtBottonNavigationBar(BuildContext context) {
    return Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Color.fromRGBO(55, 57, 84, 1.0),
            primaryColor: Colors.pink,
            textTheme: Theme.of(context).textTheme.copyWith(
                caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0)))),
        child: BottomNavigationBar(items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today), title: Container()),
          BottomNavigationBarItem(
              icon: Icon(Icons.insert_chart), title: Container()),
          BottomNavigationBarItem(
              icon: Icon(Icons.bubble_chart), title: Container()),
        ]));
  }

  Widget _myMenuRonded() {
    return Table(
      children: [
        TableRow(children: [
          _myButtonMenu(Colors.blue, Icons.dashboard, 'General'),
          _myButtonMenu(Colors.purple, Icons.directions_bus, 'Transporte'),
        ]),
        TableRow(children: [
          _myButtonMenu(Colors.pink[200], Icons.shopping_basket, 'Tienda'),
          _myButtonMenu(Colors.yellow[700], Icons.assignment, 'Compras'),
        ]),
        TableRow(children: [
          _myButtonMenu(
              Colors.blue[300], Icons.movie_filter, 'Entretenimiento'),
          _myButtonMenu(Colors.greenAccent, Icons.beach_access, 'Viajes'),
        ]),
      ],
    );
  }

  Widget _myButtonMenu(Color color, IconData icon, String text) {
    return ClipRRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
        child: Container(
          height: 180.0,
          margin: EdgeInsets.all(15.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Color.fromRGBO(62, 66, 107, 0.7)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(height: 5.0),
              CircleAvatar(
                backgroundColor: color,
                radius: 35.0,
                child: Icon(icon, color: Colors.white, size: 30.0),
              ),
              Text(
                text,
                style: TextStyle(color: color),
              ),
              SizedBox(height: 5.0)
            ],
          ),
        ),
      ),
    );
  }
}
