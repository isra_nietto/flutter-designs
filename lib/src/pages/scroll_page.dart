import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView(
      scrollDirection: Axis.vertical,
      children: [
        PrincipalPage(),
        WelcomePage(),
      ],
    ));
  }
}

class PrincipalPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [_colorView(), _imageBackgrount(), _myContent()],
    );
  }
}

Widget _colorView() {
  return Container(
    width: double.infinity,
    height: double.infinity,
    color: Color.fromRGBO(108, 192, 218, 1.0),
  );
}

Widget _imageBackgrount() {
  return Container(
    width: double.infinity,
    height: double.infinity,
    child: Image(
      image: AssetImage('assets/img/scroll-1.png'),
      fit: BoxFit.cover, // imagen en toda la pantalla
    ),
  );
}

Widget _myContent() {
  final styleGrade = TextStyle(color: Colors.white, fontSize: 50.0);
  return SafeArea(
    child: Column(
      children: [
        SizedBox(
          height: 20.0,
        ),
        Text('11º', style: styleGrade),
        Text('Jueves', style: styleGrade),
        Expanded(child: Container()),
        Icon(
          Icons.keyboard_arrow_down,
          size: 70.0,
          color: Colors.white,
        )
      ],
    ),
  );
}

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color.fromRGBO(108, 192, 218, 1.0),
      child: Center(
        child: RaisedButton(
          shape: StadiumBorder(),
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          onPressed: () {},
          child: Text(
            'Bienvenido',
            style: TextStyle(fontSize: 25.0),
          ),
          color: Colors.blueAccent,
          textColor: Colors.white,
        ),
      ),
    );
  }
}
