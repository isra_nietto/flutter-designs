import 'package:flutter/material.dart';

class BasicPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          MyLandscapa(),
          CreateTitles(),
          SizedBox(height: 20.0),
          CreateActions(),
          SizedBox(height: 30.0),
          MyCreateText(),
          MyCreateText(),
          MyCreateText(),
        ],
      ),
    ));
  }
}

class MyLandscapa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image(
        image: NetworkImage(
            'https://media-exp1.licdn.com/dms/image/C561BAQGEbvT3SFyR9Q/company-background_10000/0?e=2159024400&v=beta&t=hI9WfHDdxBHKVfJmjvSggOVF8VBYIwilVWHqR_ChmdM'));
  }
}

class CreateTitles extends StatelessWidget {
  final titleStyle = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);
  final subTitleStyle = TextStyle(fontSize: 18.0, color: Colors.grey);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          children: [
            Expanded(
              // expanded toma todo el espacio que queda
              // dependiendo de los otros witgets
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Oeschinen Lake Campground',
                    style: titleStyle,
                  ),
                  SizedBox(
                    height: 4.0,
                  ),
                  Text(
                    'Kandersteg, Switzerland',
                    style: subTitleStyle,
                  )
                ],
              ),
            ),
            Icon(Icons.star, color: Colors.red, size: 30.0),
            Text(
              '41',
              style: TextStyle(fontSize: 20.0),
            )
          ],
        ),
      ),
    );
  }
}

class CreateActions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        IconTextColumn(icon: Icons.call, text: 'CALL'),
        IconTextColumn(icon: Icons.near_me, text: 'ROUTE'),
        IconTextColumn(icon: Icons.share, text: 'SHARE'),
      ],
    );
  }
}

class IconTextColumn extends StatelessWidget {
  IconData icon;
  String text;

  IconTextColumn({Key key, this.icon, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          this.icon,
          color: Colors.blueAccent,
        ),
        SizedBox(height: 10.0),
        Text(
          this.text,
          style: TextStyle(fontSize: 15.0, color: Colors.blue),
        )
      ],
    );
  }
}

class MyCreateText extends StatelessWidget {
  const MyCreateText({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        child: Text(
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          textAlign: TextAlign.justify,
          style: TextStyle(
            fontSize: 17.0,
          ),
        ),
      ),
    );
  }
}
