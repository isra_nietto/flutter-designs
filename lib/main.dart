import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:designs/src/pages/dashboard_page.dart';
import 'package:designs/src/pages/basic_page.dart';
import 'package:designs/src/pages/scroll_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /// cambiar el status bar
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.white));

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Diseños',
      initialRoute: 'dashboard',
      routes: {
        'basico': (BuildContext context) => BasicPage(),
        'scroll': (BuildContext context) => ScrollPage(),
        'dashboard': (BuildContext context) => DashboardPage()
      },
    );
  }
}
